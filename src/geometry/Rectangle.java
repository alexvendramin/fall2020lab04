package geometry;

public class Rectangle implements Shape{
	private double length;
	private double width;
	
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	public double getLength() {
		return this.length;
	}
	public double getWidth() {
		return this.width;
	}
	
	public double getArea() {
		double area = 0;
		area = area + this.length;
		area = area * this.width;
		return area;
	}
	public double getPerimeter() {
		double perimeter = 0;
		perimeter = perimeter + this.length;
		perimeter = perimeter + this.length;
		perimeter = perimeter + this.width;
		perimeter = perimeter + this.width;
		return perimeter;
	}
}
