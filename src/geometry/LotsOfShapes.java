package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(1,2);
		shapes[1] = new Rectangle(3,4);
		shapes[2] = new Circle(5);
		shapes[3] = new Circle(6);
		shapes[4] = new Square(7);
		
		for(int i = 0; i < shapes.length; i++) {
			switch(i) {
			case 0:
			case 1:
				System.out.println("Rectangle");
				break;
			case 2:
			case 3:
				System.out.println("Circle");
				break;
			case 4:
				System.out.println("Square");
				break;
			default:
				System.out.println("NOT A SHAPE");
			}
			System.out.println("area = " + shapes[i].getArea());
			System.out.println("perimeter = " + shapes[i].getPerimeter());
		}
	}

}