package geometry;

public class Circle implements Shape{
	private double radius;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	public double getArea() {
		double area = 0;
		area = area + this.radius;
		area = area * Math.PI;
		area = area * area;
		return area;
	}
	public double getPerimeter() {
		double perimeter = 0;
		perimeter = perimeter + this.radius;
		perimeter = perimeter * Math.PI;
		perimeter = perimeter * 2;
		return perimeter;
	}
}
