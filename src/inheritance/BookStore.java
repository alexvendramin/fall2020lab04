package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new ElectronicBook("Freaking Epic Title", "Me",123);
		books[1] = new Book("Lord Of The Rings", "You");
		books[2] = new ElectronicBook("Harry Potter", "Her", 456);
		books[3] = new Book("A Book", "Him");
		books[4] = new Book("Not A Book", "Them");
		for(int i = 0; i < books.length; i++) {
			System.out.println(books[i].toString());
		}
	}

}
