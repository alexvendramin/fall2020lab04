package inheritance;

public class ElectronicBook extends Book{
	private int numberBytes;
	
	public ElectronicBook(String title, String author, int numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}
	
	public String toString() {
		String fromBase = super.toString();
		fromBase = fromBase + ", number of bytes = " + numberBytes;
		return fromBase;
	}
}
